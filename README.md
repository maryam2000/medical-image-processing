# Medical Image Processing

- In my Medical Images Analysis and Processing (MIAP) course, I implemented several denoising and segmentation methods in MATLAB.
- Denoising Methods: Non Local Mean (NLM), Bilateral Filtering, Total Variation
- Segmentation Methods: Fuzzy C-Means (FCM), K-means, Gaussian Mixture Model (GMM), Gradient Vector Flow (GVF), Basic Snake, Adaptive Regularized Kernel-based Fuzzy C-Means (ARKFCM)
